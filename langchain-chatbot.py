from langchain_openai import ChatOpenAI
from langchain.chains import LLMChain
from langchain.prompts import (
    MessagesPlaceholder,
    HumanMessagePromptTemplate,
    ChatPromptTemplate,
)
from langchain.memory import ConversationSummaryMemory
from dotenv import load_dotenv

# Fetch OPENAI_API_KEY from .env file
load_dotenv()

chat = ChatOpenAI(verbose=True)
memory = ConversationSummaryMemory(
    memory_key="messages",  # Memory key to identify the conversation history
    return_messages=True,  # Return conversation messages along with other information
    llm=chat,  # Language model for interaction
)

prompt = ChatPromptTemplate(
    input_variables=["content", "messages"],  # Input variables for the prompt
    messages=[
        # Placeholder for conversation messages
        MessagesPlaceholder(variable_name="messages"),
        HumanMessagePromptTemplate.from_template(
            "{content}"),  # Template for user's input content
    ],
)

chain = LLMChain(
    llm=chat,  # Language model for interaction
    prompt=prompt,  # Prompt template
    memory=memory,  # Conversation memory
    verbose=True,  # Print verbose information during interaction
)

# Infinite loop for continuous interaction
while True:
    content = input(">> ")  # Get user input
    result = chain({"content": content})  # Perform language model interaction
    print(result["text"])  # Print the generated text from the language model
