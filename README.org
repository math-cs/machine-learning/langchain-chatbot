* ChatBot: Memory Management with LangChain

This Python script interacts with the LangChain library and
OpenAI's language model to create a chat-based conversational
experience. It uses the LangChain library to manage conversation
history, generate prompts for the language model, and handle
continuous interactions with the user. Conversation history is stored
and retrieved using the /ConversationSummaryMemory/ class.

** Sample Output:
Run the script with

python langchain-chatbot.py

Enter content when prompted *(>>)* to interact with the language model.
#+BEGIN_EXAMPLE
>> how many calories are in an apple?


> Entering new LLMChain chain...
Prompt after formatting:
System:
Human: how many calories are in an apple?

> Finished chain.
The number of calories in an apple can vary depending on its size and type. On average, a medium-sized apple (approximately 182 grams) contains about 95 calories.
>> and which fruit has the most?


> Entering new LLMChain chain...
Prompt after formatting:
System: The human asks how many calories are in an apple. The AI explains that the number of calories can vary depending on the size and type of the apple, but on average, a medium-sized apple contains about 95 calories.
Human: and which fruit has the most?

> Finished chain.
The fruit with the most calories per serving is typically the avocado. A medium-sized avocado contains around 240-280 calories. However, it's important to note that
avocados are also a good source of healthy fats, fiber, and various nutrients, making them a nutritious addition to a balanced diet.
>> how does that compare to a banana?


> Entering new LLMChain chain...
Prompt after formatting:
System: The human asks how many calories are in an apple. The AI explains that the number of calories can vary depending on the size and type of the apple, but on average, a medium-sized apple contains about 95 calories. The human then asks which fruit has the most calories, and the AI responds that the fruit with the most calories per serving is typically the avocado, containing around 240-280 calories. However, the AI adds that avocados are also a good source of healthy fats, fiber, and various nutrients, making them a nutritious addition to a balanced diet.
Human: how does that compare to a banana?

> Finished chain.
On average, a medium-sized banana contains about 105 calories, which is slightly higher than the calorie content of a medium-sized apple. Bananas are also a good source of carbohydrates, potassium, vitamin C, and dietary fiber. While they may have slightly more calories than an apple, bananas still provide numerous health benefits and can be a nutritious snack choice.
#+END_EXAMPLE
